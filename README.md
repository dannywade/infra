# My first CI/CD pipeline

## Part 1
Created initial CI/CD pipeline with a single playbook to configure syslog settings on a Cisco IOS device.

## Part 2
Added an additional device and broke out the variables (i.e. IP addresses) to host and group vars. I also created roles for each network requirement. The idea is that the roles will identify the different configuration items needed on a network device (i.e. Device mgmt, L3 routing protocol, network services, etc.). So far, I've configured roles for device management (mgmt IP (Lo0) and SNMP) and a role for EIGRP (this could probably be extended to a L3 routing role and include more L3 routing protocols).

After defining the different roles, I created different playbooks for the different devices in the network: WAN routers, L3 switches, L2 switches, etc. The idea is that you can define a new device in your inventory and store its variables in a new host_vars file, then run a playbook that includes all the necessary roles (network configuration) that is needed on the device.

The logic I just defined may seem different. Normally, you would consider the different devices in the network (WAN routers, switches, etc.) as your different roles. However, I built this repo based on what currently makes sense to me. Each role will define a piece of a network configuration. I related this to how servers are traditionally built using Ansible - each service hosted on a server (Apache, DB services, etc.) is considered a role. I'm not ruling out that my logic will change - I just wanted to explain where I'm at with my current logic.



*All credit to nwmichl at nwmichl.net for his great walkthrough that got me started!*
